import concurrent.futures as cf
import logging
import names
import traceback


class ChainExecutor():
    def __init__(self, preChainFn=None, postChainFn=None):
        # Allow only one thread at a time.
        self.preChainFn = preChainFn
        self.postChainFn = postChainFn
        self.pool = cf.ThreadPoolExecutor(max_workers=1)
        self.logger = logging.getLogger("general_logs.ChainExecutor")
        self.f = None

    def runSingle(self, fn, *args, cb=None, **kwargs):
        chain = self.newChain()
        chain.addCall(fn, *args, **kwargs)
        self.runChain(chain, cb=cb)

    def newChain(self):
        return self.Chain(names.get_first_name())

    def callback(self, future):
        try:
            result = future.result()

        except Exception as e:
            self.logger.error("Thread failed with exception: %s \n %s" %
                              (e, traceback.format_exc))
            result = None

        else:
            self.logger.debug("Thread terminated normally.")

        if self.postChainFn is not None:
            self.postChainFn()

        return result

    def runChain(self, chain, cb=None, reactive=False):
        if self.preChainFn is not None:
            self.preChainFn()

        if self.f is not None:
            # In reactive mode, do not block and skip chains if busy
            if not self.f.done() and reactive:
                return

        self.f = self.pool.submit(chain.run)

        if cb is not None:
            self.f.add_done_callback(cb)
        else:
            self.f.add_done_callback(self.callback)

    class Chain():
        def __init__(self, name):
            # The list of chained calls
            self.calls = []

            # The future object under which this chain runs
            self.future = None

            # Callback after chain finish
            self.name = name
            self.errCnt = 0

            self.logger = logging.getLogger("general_logs.ChainExecutor.Chain")

        def getFirstMatchingCall(self, fn):
            for c in self.calls:
                if c.fn == fn:
                    return c

        def getMatchingCalls(self, fn):
            matchingCalls = []

            for c in self.calls:
                if c.fn is fn:
                    matchingCalls.append(c)

            return matchingCalls

        def addCall(self, fn, *args, **kwargs):
            self.calls.append(self.Call(fn, *args, **kwargs))

        def run(self):
            self.logger.debug("Start chain \"%s\"..." % self.name)
            for cidx, c in enumerate(self.calls):
                self.logger.debug("Running function %s in chain \"%s\" (call %d/%d)" %
                                  (c.fn.__name__, self.name, cidx+1, len(self.calls)))
                try:
                    c.result = c.fn(*c.args, **c.kwargs)
                except Exception as e:
                    self.logger.debug("Call to function %s failed in chain \"%s\": %s" % (
                        c.fn.__name__, self.name, e))
                    self.errCnt += 1

            if self.errCnt == 0:
                self.logger.debug(
                    "Chain \"%s\" finished without errors." % self.name)
            else:
                self.logger.debug("Chain \"%s\" finished %d error(s)." %
                                  (self.name, self.errCnt))

            return self

        class Call():
            def __init__(self, fn, *args, **kwargs):
                self.fn = fn
                self.args = args
                self.kwargs = kwargs
                self.result = None
